;; https://github.com/ubolonton/emacs-tree-sitter/issues/68

;; https://stackoverflow.com/a/4201022
(defmacro comment (&rest body)
  "Comment out one or more s-expressions, i.e. BODY."
  nil)

;; setup via straight.el:

(comment

(straight-use-package
 '(ts-cycle-hl :host gitlab
               :repo "sogaiu/ts-cycle-hl"
               :files ("*.el")))

;; XXX: change value within `:config` section appropriately
(use-package ts-cycle-hl
  :straight t
  :config
  (setq ts-cycle-hl--custom-file-paths-alist
        '((clojure .
          ("~/src/emacs-ts-queries/clojure-tonsky-alabaster/highlights.scm"))
          (janet .
          ("~/src/emacs-ts-queries/janet-tonsky-alabaster/highlights.scm")))))

)

;; usage:

;; use the command `ts-cycle-hl' to cycle through different highlight sets

(require 'tree-sitter)

;;; Code:

;; XXX: likely want to configure this value, see above for example
(defvar ts-cycle-hl--custom-file-paths-alist)

(defun ts-cycle-hl--init-current-index-alist ()
  "Initialize all current index values."
  (mapcar (lambda (pair)
            (let ((lang-sym (cdr pair)))
              (cons lang-sym 0)))
          tree-sitter-major-mode-language-alist))

(defvar ts-cycle-hl--current-index-alist
  (ts-cycle-hl--init-current-index-alist))

(defun ts-cycle-hl--current-index ()
  "Determine index for current major mode language."
  ;; XXX: (tsc--lang-symbol tree-sitter-language) can replace alist-get below
  (when-let ((lang-sym (alist-get major-mode
                                  tree-sitter-major-mode-language-alist)))
    (alist-get lang-sym ts-cycle-hl--current-index-alist)))

(defun ts-cycle-hl--set-current-index (new-val)
  "Set index to NEW-VAL for current major mode language."
  ;; XXX: (tsc--lang-symbol tree-sitter-language) can replace alist-get below
  (when-let ((lang-sym (alist-get major-mode
                                  tree-sitter-major-mode-language-alist)))
    (when (ts-cycle-hl--current-index)
      (setf (alist-get lang-sym ts-cycle-hl--current-index-alist)
            new-val))))

(defun ts-cycle-hl--init-file-paths-alist ()
  "Determine initial highlight file path alist."
  (mapcar (lambda (pair)
            (let* ((lang-sym (cdr pair))
                   (file-path (tree-sitter-langs--hl-query-path lang-sym)))
              (cons lang-sym (list file-path))))
          tree-sitter-major-mode-language-alist))

(defun ts-cycle-hl--refresh-file-paths-alist ()
  "Compute fresh highlight file path alist."
  (mapcar
   (lambda (pair)
     (let* ((lang-sym (car pair))
            (file-paths-list (cdr pair)))
       (if-let ((custom-paths-list
                 (alist-get lang-sym
                            ts-cycle-hl--custom-file-paths-alist)))
           (cons lang-sym (append file-paths-list
                                  custom-paths-list))
         (cons lang-sym file-paths-list))))
   (ts-cycle-hl--init-file-paths-alist)))

(defvar ts-cycle-hl--file-paths-alist)

(defun ts-cycle-hl--current-file-paths ()
  "Determine highlight file paths for current major mode language."
  ;; XXX: (tsc--lang-symbol tree-sitter-language) can replace alist-get below
  (when-let ((lang-sym (alist-get major-mode
                                  tree-sitter-major-mode-language-alist)))
    (alist-get lang-sym ts-cycle-hl--file-paths-alist)))

;;;###autoload
(defun ts-cycle-hl ()
  "Cycle to logically next highlight patterns and highlight based on result."
  (interactive)
  (setq ts-cycle-hl--file-paths-alist
        (ts-cycle-hl--refresh-file-paths-alist))
  (when tree-sitter-hl-mode
    (tree-sitter-hl-mode -1))
  (ts-cycle-hl--set-current-index
   (1+ (ts-cycle-hl--current-index)))
  (when (>= (ts-cycle-hl--current-index)
            (length (ts-cycle-hl--current-file-paths)))
    (ts-cycle-hl--set-current-index 0))
  ;; tree-sitter-langs--hl-default-patterns
  (let ((index (ts-cycle-hl--current-index))
        (file-paths (ts-cycle-hl--current-file-paths)))
    (setq tree-sitter-hl-default-patterns
          (condition-case nil
              (with-temp-buffer
                (insert-file-contents (nth index file-paths))
                (goto-char (point-max))
                (insert "\n")
                (buffer-string))
            (file-missing nil))))
  (tree-sitter-hl-mode))

(provide 'ts-cycle-hl)
;;; ts-cycle-hl.el ends here
